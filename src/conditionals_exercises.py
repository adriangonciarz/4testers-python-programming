def display_speed_information(speed):
    # if speed <= 50:
    #     print('Thank you, your speed is below limit! :)')
    # else:
    #     print('Slow down! :(')
    if speed > 50:
        print('Slow down! :(')
    else:
        print('Thank you, your speed is below limit! :)')


def check_if_the_weather_is_normal(celsius_degree, pressure_in_hectopascals):
    if celsius_degree == 0 and pressure_in_hectopascals == 1013:
        print(True)
    else:
        print(False)


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print('Your speed was:', speed)
    if speed > 100:
        print('You just lost your driving license :(')
    elif speed > 50:
        print(f'You just got a fine! Fine amount {calculate_fine_amount(speed)} :(')
    else:
        print('Thank you, your speed is fine')


def get_grade_info(grade):
    # if not (isinstance(grade, float) or isinstance(grade, int)):
    #     return 'N/A'
    if grade < 2 or grade > 5:
        return 'N/A'
    elif grade >= 4.5:
        return 'Bardzo dobry'
    elif grade >= 4.0:
        return 'dobry'
    elif grade >= 3.0:
        return 'dostateczny'
    else:
        return 'niedostateczny'


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

    check_if_the_weather_is_normal(0, 1013)
    check_if_the_weather_is_normal(1, 1013)
    check_if_the_weather_is_normal(1, 1014)
    check_if_the_weather_is_normal(0, 1014)

    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(50)
    print_the_value_of_speeding_fine_in_built_up_area(51)
    print_the_value_of_speeding_fine_in_built_up_area(70)

    print(get_grade_info(5))
    print(get_grade_info(4.5))
    print(get_grade_info(4))
    print(get_grade_info(3))
    print(get_grade_info(2))
    print(get_grade_info(1))
    print(get_grade_info(6))
