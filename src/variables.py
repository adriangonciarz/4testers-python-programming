first_name = "Adrian"
last_name = "Gonciarz"
age = 34

print(first_name)
print(last_name)
print(age)

# I am defining a set of variables describing my dog
name = "Panela"
coat_color = 'Black/brown'
age_in_months = 5
number_of_paws = 4
weight_in_kilograms = 5.1
height_in_centimeters = 23.5
is_male = False
has_chip = True
is_vaccinated = True

print(name, age_in_months, weight_in_kilograms, number_of_paws, coat_color, is_male)

my_bank_account_total = 6_000_000
my_friends_account_total = 5e12

print(my_bank_account_total)
print(my_friends_account_total)

# Below I describe my friendship details as a set of variables
friend_name = 'Tomek'
friend_age = 33
friend_number_of_animals = 2
friend_has_driving_license = False
friendship_duration_in_years = 3.5

print("Friend's name:", friend_name, sep='\t')

print(friend_name, friend_age, friend_number_of_animals, friend_has_driving_license, friendship_duration_in_years, sep=', ')

