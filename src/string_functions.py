def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widzieć w naszym mieście: {city}!")


def get_greetings_for_a_person_in_the_city(person_name, city):
    return f"Witaj {person_name}! Miło Cię widzieć w naszym mieście: {city}!"


def generate_email_address(person_first_name, person_last_name):
    print(f"{person_first_name.lower()}.{person_last_name.lower()}@4testers.pl")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Kasia", "Szczecin")
    print(get_greetings_for_a_person_in_the_city("Adam", "Poznań"))
    generate_email_address("Janusz", "Nowak")
    generate_email_address("Barbara", "Kowalska")
