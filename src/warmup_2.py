def calculate_square_of_the_number(number):
    return number * number


def convert_temperature_in_celsius_to_fahrenheit(temperature_in_celsius):
    return temperature_in_celsius * 1.8 + 32


def calculate_volume_of_cuboid(side1, side2, side3):
    return side1 * side2 * side3


if __name__ == '__main__':
    # Zadanie 1
    print(calculate_square_of_the_number(0))
    print(calculate_square_of_the_number(16))
    print(calculate_square_of_the_number(2.55))

    # Zadanie 2
    print(convert_temperature_in_celsius_to_fahrenheit(20))

    # Zadanie 3
    print(calculate_volume_of_cuboid(3, 5, 7))
