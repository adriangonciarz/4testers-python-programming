movies = ['Dune', 'Star Wars', 'Blade Runner', 'Stalker', 'Lost Highway']

last_movie = movies[-1]
movies.append('LOTR')
movies.append('Titanic')
print(len(movies))
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'Top Gun 2')
print(movies)

emails = ['a@example.com', 'b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@exapmple.com')

friend = {
    "first_name": "Tomasz",
    "age": 43,
    "hobby": ["sport", "motoryzacja"]
}

friend_hobbies = friend["hobby"]
print("Hobbies of my friend:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")
friend["hobby"].append("football")
print(friend)
friend["married"] = True
friend["age"] = 44
print(friend)

