from src.employee_management import generate_seniority_years


def test_generated_seniority_years_is_gtoe_zero():
    test_seniority_years = generate_seniority_years()
    assert test_seniority_years >= 0


def test_generated_seniority_years_is_ltoe_forty():
    test_seniority_years = generate_seniority_years()
    assert test_seniority_years <= 40


def test_generated_seniority_years_is_an_integer():
    test_seniority_years = generate_seniority_years()
    assert isinstance(test_seniority_years, int)
